Thank you for downloading. :)
==============================================================
Contact us :)
==============================================================
*** UpWork (Only For Graphic Design): https://www.upwork.com/o/profiles/users/_~0180eb039dfa707571/

Twitter: https://twitter.com/AtsparBd
Facebook: https://www.facebook.com/atsparbd/

***Team Leader Skype: smahatik ***

We are also in Fiverr, Freelancer, 99design, Design Crowd


Hi Hope you doing well,
 
We are a 20 member team, and we do all kinds of web and print materials design.

Print Materials
------------------------------
* Office Identity (Business Card, Letter Head, Envelope, Folder etc.)
* Flyer
* Logo
* Brochure
* Banner
* Label 
* T-Shirt
* Post Card
* Invitation Card etc.

Web Materials
------------------------------
* Cover Photo
* Web Template 
* Web Banner
* Google ad 
* Image Retouch
* Background Remove
* PowerPoint presentation etc.

We also do web design and customize using WordPress(4.9.4). 
If you need any kind of work that I mention above or any design that isn't there don't hesitate, just let us know.
We will give our best to satisfy you.
Client satisfaction is our only goal.

Have a great day
Thanks,
atsparbd
